import { Component, EventEmitter, Output } from '@angular/core';
import { TransferenciaService } from '../services/transferencia.service';
import { Transferencia } from '../models/transferencia.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nova-transferencia',
  templateUrl: './nova-transferencia.component.html',
  styleUrls: ['./nova-transferencia.component.scss'],
})
export class NovaTransferenciaComponent {
  @Output() aoTransferir = new EventEmitter<any>();
  valor?: number;
  destino?: string;

  constructor(
    private _transferenciaService: TransferenciaService,
    private _router: Router
  ) {}

  transferir() {
    const transferencia: Transferencia = {
      valor: this.valor,
      destino: this.destino,
    };
    //this.aoTransferir.emit(valorTransferido);
    this._transferenciaService.adicionar(transferencia).subscribe(
      (resultado) => {
        console.log(resultado);
        this.limparCampos();
        this._router.navigateByUrl('extrato');
      },
      (error) => console.log(error)
    );
  }

  private limparCampos() {
    this.valor = undefined;
    this.destino = undefined;
  }
}
