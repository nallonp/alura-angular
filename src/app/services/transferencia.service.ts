import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Transferencia } from '../models/transferencia.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class TransferenciaService {
  private _url = 'http://localhost:3000/transferencias';

  constructor(private httpClient: HttpClient) {
    this._listaTransferencias = [];
  }

  private _listaTransferencias: any[];

  get listaTransferencias(): any[] {
    return this._listaTransferencias;
  }

  todas(): Observable<Transferencia[]> {
    return this.httpClient.get<Transferencia[]>(this._url);
  }

  adicionar(transferencia: any): Observable<Transferencia> {
    this.hidratar(transferencia);
    return this.httpClient.post<Transferencia>(this._url, transferencia);
  }

  hidratar(transferencia: any) {
    transferencia.data = new Date();
  }
}
